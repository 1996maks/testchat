var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var chat = require('./routes/chat/index');


var port = process.env.PORT || 4000
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, My-Token, Authorization");
    res.header("Access-Control-Allow-Methods", "PUT,POST,DELETE");
    

    next();
});


// app.use(bodyParser.json({
//     type: function() {
//         return true;
//     }
// }));
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/', chat);

app.listen(port, function() {
    console.log('App listening at ' + port);
});

module.exports = app;