var express = require('express');
var mongojs = require('mongojs');
var db = mongojs('mongodb://Maxik:1996maks@ds227481.mlab.com:27481/chat', ['chat']);
var jwt = require('jsonwebtoken');
var router = express.Router();

router.get('/api/chat', (req, res) => {
  db.chat.find(function(err, chat) {
    if (err) {
      res.send(err);
    }
    res.json(chat);
  });
});

router.get('/api/users', (req, res) => {
  db.users.find(function(err, users) {
    if (err) {
      res.send(err);
    }
    res.json(users);
  });
});

/*
Add user to database
*/ 
router.post('/api/register', function(req, res, next) {
  var user = req.body;
  db.users.save(user, (err) => {
    if (err) {
      res.send(err);
    } else {
      res.send({
        status: "success",
        user
      })
    }
  })
});

/**
* Check if user exist
*/
router.post('/api/login', function(req, res, next) {
  console.log(req.body);
  var user = req.body;
  db.users.find({ name: user.name, password: user.password }).toArray(function(err, user) {
    if (user.length > 0) {
      jwt.sign({user}, 'secretkey', { expiresIn: '30m' }, (err, token) => {
        res.send(token);
      });
    } else {
      res.send(false);
    }
  });
});

router.post('/api/chat', verifyToken, (req, res, next) =>{
  console.log(req.body);
  var chat = req.body;
  var regExp = /^[0-9a-zA-Z]+$/;
  
  jwt.verify(req.token, 'secretkey', (err, authData) => {
    if (err) {
      res.sendStatus(403);
    } else {
      chat.username.match(regExp)&&chat.text.length <= 200 
      ? db.chat.save(chat, function(err, chat) {
        if (err) {
          res.send(err);
        } else {
          res.send({
            status: "success",
            chat
          })
        }
      })
      : res.send({"error":"Data is not valid"})
    }
  });
 
});


// Format of token : 
// Authorization: Bearer <access_token>

// Verify Token
function verifyToken(req, res, next) {
  // Get auth header value
  const bearerHeader = req.headers['authorization'];
  //check if bearer is undefined 
  if (typeof(bearerHeader !== 'undefined')) {
    // Split at the space 
    const bearer = bearerHeader.split(' ');
    // Get token from an array
    const bearerToken = bearer[1];
    // Set the token 
    req.token = bearerToken;
    // next middleware 
    next();
  } else {
    // Forbidden
    res.sendStatus(403);
  }
}

module.exports = router;